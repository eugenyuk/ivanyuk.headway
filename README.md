# Hello world web app


This is a containerized hello world web application, which is passed through
Gitlab CI/CD pipeline and is deployed to Docker Swarm cluster.

## Dockerfiles

Application consists of 2 Docker containers with nginx web server and php-fpm
application server.

nginx Dockerfile:
```
# Base image. To make sure it is always fresh, not set a version tag.
FROM	nginx
# Copy the files from host to the container
COPY	./site.conf /etc/nginx/conf.d/	
COPY	./code /code
```

php-fpm Dockerfile:
```
# Base image. To make sure it is always fresh, not set a version.
FROM php:fpm
# Make a directory inside the container.
RUN mkdir -p /code
# Copy the files from host to the container
COPY code/index.php /code/
```
So, all containers have all needed files inside.

## Nginx configuraion

```
server {
    index index.php;
    server_name ivanyuk.headway.global;
    error_log  /var/log/nginx/error.log;
    access_log /var/log/nginx/access.log;
    root /code;

    location ~ (^/$|\.php$) {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php-fpm:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
}
```
As seen from the conf file, nginx proxies all requests to / URI and php files to
php-fpm container, but processes all static files himself (if there are in /code
dir).

## Docker swarm configuration

On the host, where our application should be deployed, install docker.
By default, dockerd service doesn't listen to public address. So, to allow
connection from outside it should be configured to listen to the public address:
```
$ mkdir -p /etc/systemd/system/docker.service.d
```
Create the file /etc/systemd/system/docker.service.d/exec-start.conf:
```
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd
```
Create new configuration file for dockerd /etc/docker/daemon.json:
```
{
  "hosts": ["tcp://172.31.5.159:2376","fd://"]
}
```

To take effect changes:
```
$ systemctl daemon-reload
$ service docker restart
```

Initialize swarm mode:
```
> docker swarm init
```
As we have a singe host, it acts both as a manager as a worker.

## Workflow
1. Developer pushes merge commit from master branch

2. Gitlab reads .gitlab-ci.yml file:
```
workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID               # Execute jobs in merge request context
    - if: $CI_COMMIT_BRANCH == 'master'      # Execute jobs when a new commit is pushed to master branch
```
As this is a merge commit to the branch 'master', Gitlab reads the file further and runs CI/CD jobs.

3. On a *build* stage we log in to Gitlab containers registry, build and push our containers:
```
build:
  stage: build
  script:
    # Log in to gitlab containers registry to be able to pull/push
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    # Build & push nginx image
    - docker build -t $CI_REGISTRY/eugenyuk/ivanyuk.headway/nginx_img:$CI_COMMIT_SHA ./containers/nginx
    - docker push $CI_REGISTRY/eugenyuk/ivanyuk.headway/nginx_img:$CI_COMMIT_SHA
    # Build & push php-fpm image
    - docker build -t $CI_REGISTRY/eugenyuk/ivanyuk.headway/php-fpm_img:$CI_COMMIT_SHA ./containers/php-fpm
    - docker push $CI_REGISTRY/eugenyuk/ivanyuk.headway/php-fpm_img:$CI_COMMIT_SHA
  # This tag is needed to process the job using Gitlab publi runner
  tags:
    - docker
```



4. On a *deploy* stage we deploy built containers to docker swarm on our server ivanyuk.headway.global:2376:
```
deploy-to-swarm:
  stage: deploy
  variables:
    DOCKER_HOST: tcp://ivanyuk.headway.global:2376
  script:
    # Log in to gitlab containers registry to be able to pull containers
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    # Deploy or update an existing stack according to docker-compose-deploy-swarm.yml
    - docker stack deploy -c docker-compose-deploy-swarm.yml env_name --with-registry-auth
  environment:
    name: production
    # It exposes buttons in various places in GitLab which when clicked take you to the defined URL.
    url: http://ivanyuk.headway.global
  # Start deploy-to-swarm job manually
  when: manual
  # This tag is needed to process the job using Gitlab public runner
  tags:
    - docker
```
*deploy* stage is configured in manual mode, which means we should go to Gitlab CI/CD -> Jobs interface and press *Play* button.
